// use an object literal: {} to create an object representing a user
	//encapsulatio
	//whenever we add properties or methods to an object, we are performing encapsulation
	// the organization of informatuon (as properties) and behavior (as methods) to belong to the object that encapsulates them
	// the scope of encapsulation is denoted by object literals

let studentOne = {
	name: "John",
	email: "john@mail.com",
	grades : [89, 84, 78, 88],

	//methods
		// add te functionalities available to a student as object methods
	login(){
		console.log(`${this.email} has logged in`)
	},
	logout(){
		console.log(`${this.email} has logged out`)
	},
	listGrades(){
		console.log(`${this.name}'s quarterly grade averages are ${this.grades}`)
	},
	// Mini activity
	gradeAverage(){
		let numbers = this.grades;
		let sum = numbers.reduce((accumulator, currentValue) => {
		    return accumulator + currentValue;
		  }, 0);
		let avg = sum / numbers.length;
		return avg;
	},
	isPassing(){
		if(this.gradeAverage()>=85){
			return true
		}else{
			return false
		}
	},
	willPassWithHonors(){
		if(this.gradeAverage()>=90){
			return true
		}else{
			return false
		}
	}


}
// log the content of studentOne's encapsulated information in the console
console.log(`studentOne's name: ${studentOne.name} `);
console.log(`studentOne's email: ${studentOne.email} `);
console.log(`studentOne's grades: ${studentOne.grades} `);
console.log(studentOne)