/*

	1. What is the term given to unorganized code that's very hard to work with?
		- Spaghetti code
	2. How are object literals written in JS?
		- {}
	3. What do you call the concept of organizing information and functionality to belong to an object?
		- Object oriented programming
	4. If studentOne has a method named enroll(), how would you invoke it?
		- studentOne.enroll()
	5. True or False: Objects can have objects as properties.
		- True
	6. What is the syntax in creating key-value pairs?
		- key:value
	7. True or False: A method can have no parameters and still work.
		- True
	8. True or False: Arrays can have objects as elements.
		- True
	9. True or False: Arrays are objects.
		- True
	10. True or False: Objects can have arrays as properties.
		- True


*/

//1. Translate the other students from our boilerplate code into their own respective objects.

//2. Define a method for EACH student object that will compute for their grade average (total of grades divided by 4)

//3. Define a method for all student objects named willPass() that returns a Boolean value indicating if student will pass or fail.
//For a student to pass, their ave. grade must be greater than or equal to 85.

//4. Define a method for all student objects named willPassWithHonors() that returns true if ave. grade is greater than or equal to 90,
//false if >= 85 but < 90, and undefined if < 85 (since student will not pass).

//5. Create an object named classOf1A with a property named students which is an array containing all 4 student objects in it.

//6. Create a method for the object classOf1A named countHonorStudents() that will return the number of honor students.

//7. Create a method for the object classOf1A named honorsPercentage() that will return the % of honor students from the batch's total number of students.

//8. Create a method for the object classOf1A named retrieveHonorStudentInfo()
//that will return all honor students' emails and ave. grades as an array of objects.

//9. Create a method for the object classOf1A named sortHonorStudentsByGradeDesc()
//that will return all honor students' emails and ave. grades as an array of objects sorted in descending order based on their grade averages.

let studentOne = {
	name: "John",
	email: "john@mail.com",
	grades : [89, 84, 78, 88],

	//methods
		// add te functionalities available to a student as object methods
	login(){
		console.log(`${this.email} has logged in`)
	},
	logout(){
		console.log(`${this.email} has logged out`)
	},
	listGrades(){
		console.log(`${this.name}'s quarterly grade averages are ${this.grades}`)
	},
	// Mini activity
	computeAve(){
		let numbers = this.grades;
		let sum = numbers.reduce((accumulator, currentValue) => {
		    return accumulator + currentValue;
		  }, 0);
		let avg = sum / 4;
		return avg;
	},
	willPass(){
		if(this.computeAve()>=85){
			return true
		}else{
			return false
		}
	},
	willPassWithHonors(){
		if(this.computeAve()>=90){
			return true
		}else if (this.computeAve()>=85&&this.computeAve()<90){
			return false
		}
	}

}
let studentTwo = {
	name: "Joe",
	email: "joe@mail.com",
	grades : [78, 82, 79, 85],

	//methods
		// add te functionalities available to a student as object methods
	login(){
		console.log(`${this.email} has logged in`)
	},
	logout(){
		console.log(`${this.email} has logged out`)
	},
	listGrades(){
		console.log(`${this.name}'s quarterly grade averages are ${this.grades}`)
	},
	// Mini activity
	computeAve(){
		let numbers = this.grades;
		let sum = numbers.reduce((accumulator, currentValue) => {
		    return accumulator + currentValue;
		  }, 0);
		let avg = sum / 4;
		return avg;
	},
	willPass(){
		if(this.computeAve()>=85){
			return true
		}else{
			return false
		}
	},
	willPassWithHonors(){
		if(this.computeAve()>=90){
			return true
		}else if (this.computeAve()>=85&&this.computeAve()<90){
			return false
		}
	}

}

let studentThree = {
	name: "Jane",
	email: "jane@mail.com",
	grades : [87, 89, 91, 93],

	//methods
		// add te functionalities available to a student as object methods
	login(){
		console.log(`${this.email} has logged in`)
	},
	logout(){
		console.log(`${this.email} has logged out`)
	},
	listGrades(){
		console.log(`${this.name}'s quarterly grade averages are ${this.grades}`)
	},
	// Mini activity
	computeAve(){
		let numbers = this.grades;
		let sum = numbers.reduce((accumulator, currentValue) => {
		    return accumulator + currentValue;
		  }, 0);
		let avg = sum / 4;
		return avg;
	},
	willPass(){
		if(this.computeAve()>=85){
			return true
		}else{
			return false
		}
	},
	willPassWithHonors(){
		if(this.computeAve()>=90){
			return true
		}else if (this.computeAve()>=85&&this.computeAve()<90){
			return false
		}
	}

}

let studentFour = {
	name: "Jessie",
	email: "jessie@mail.com",
	grades : [91, 89, 92, 93],

	//methods
		// add te functionalities available to a student as object methods
	login(){
		console.log(`${this.email} has logged in`)
	},
	logout(){
		console.log(`${this.email} has logged out`)
	},
	listGrades(){
		console.log(`${this.name}'s quarterly grade averages are ${this.grades}`)
	},
	// Mini activity
	computeAve(){
		let numbers = this.grades;
		let sum = numbers.reduce((accumulator, currentValue) => {
		    return accumulator + currentValue;
		  }, 0);
		let avg = sum / 4;
		return avg;
	},
	willPass(){
		if(this.computeAve()>=85){
			return true
		}else{
			return false
		}
	},
	willPassWithHonors(){
		if(this.computeAve()>=90){
			return true
		}else if (this.computeAve()>=85&&this.computeAve()<90){
			return false
		}
	}

}

let classOf1A = {
	students : [studentOne, studentTwo, studentThree, studentFour],
	countHonorStudents(){
		let result = 0; 
		this.students.forEach(student=>{
		 	if(student.willPassWithHonors()){
		 		result++
		 	}
		});
		return result
	},
	honorsPercentage(){
		return ((this.countHonorStudents()/4)*100)
	},
	retrieveHonorStudentInfo(){
		studs=[]
		this.students.forEach(student=>{
		 	if(student.willPassWithHonors()){
		 		studs.push({"email":student.email, "aveGrade": student.computeAve()})
		 	}
		});
		return studs;
	},
	sortHonorStudentsByGradeDesc(){
		return this.retrieveHonorStudentInfo().sort((s1, s2) => (s1.aveGrade < s2.aveGrade) ? 1 : (s1.aveGrade > s2.aveGrade) ? -1 : 0)
	}

}